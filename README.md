# README

## How to install project


1. Install Ruby on Rails (Versionen Ruby=2.5.5; Rails=6.0.4)
- Tutorial: https://medium.com/ruby-on-rails-web-application-development/how-to-install-rubyonrails-on-windows-7-8-10-complete-tutorial-2017-fc95720ee059#4cc2
- RubyInstaller: https://rubyinstaller.org/

2. Install Postges as Database
https://www.postgresql.org/download/windows/

3. Download project

4. go to project folder

5. Run `bundle install` in terminal

6. change postgresql database username and password in `database.yml`
-> https://gitlab.com/Jamo97/rest-api/-/blob/main/config/database.yml

7. setup database: `rails db:setup`

8. migrate databse: `rails db:migrate`

9. start Rails server: `rails s`

10. open http://localhost:3000/ in browser

## Routes

| Verb | URI Pattern | Action |
| ------ | ------ | ------ |
| GET | /api/users | Get all users |
| POST | /api/users | Create new user |
| GET | /api/users/:id | get user by id |
| PUT | /api/users/:id | update user by id |
| DELETE | /api/users/:id | delete user by id |

# Parameters to create and update users
_Run in an API Client, like Postman_

`{
    "username": "UserName",
    "first_name": "FirstName",
    "last_name": "LastNa,e",
    "password": "this_is_secret",
    "address_attributes": {
        "street": "Street",
        "number": "StreetNumber",
        "zip": 12345,
        "city": "City"
    }
}`

## Code file which executes the Create, Read, Update and Delete commands.

https://gitlab.com/Jamo97/rest-api/-/blob/main/app/controllers/api/users_controller.rb
