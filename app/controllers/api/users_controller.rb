class Api::UsersController < ApplicationController

    # GET /users
    def index
      @users = User.all
      render json: @users, include: [:address]
    end

    # GET /users/:id
    def show
        @user = User.find(params[:id])
        render json: @user, include: [:address]
    end

    # POST /users
    def create
        @user = User.new(user_params)
        if @user.save
            render json: @user
        else
            render error: { error: 'Unable to create User' }, status: 400
        end
    end

    # PUT /users/:id
    def update
        @user = User.find(params[:id])
        if @user
            @user.update(user_params)
            render json: { message: 'User succesfully updated' }, status: 200
        else
            render error: { error: 'Unable to update User' }, status: 400
        end
    end

    # DELETE /useres/:id
    def destroy
        @user = User.find(params[:id])
        if @user
            @user.destroy
            render json: { message: 'User succesfully deleted.'}, status: 200
        else
            render error: { error: 'Unable to delete User' }, status: 400
        end
    end

    private

    def user_params
        params.require(:user).permit(:username, :first_name, :last_name, :password, address_attributes: [:id, :street, :number, :zip, :city])
    end

end
